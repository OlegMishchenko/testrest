<?php

namespace api\controllers;

use yii\rest\ActiveController;

class TaskController extends ActiveController
{

    public $modelClass = 'common\models\Task';

}
